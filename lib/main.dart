import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:path_provider/path_provider.dart';
import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';

typedef void OnError(Exception exception);

const kUrl =
    "http://dl.nex1music.ir/1397/11/13/Behnam%20Bani%20-%20Toyi%20Entekhabam.mp3?time=1550834085&filename=/1397/11/13/Behnam%20Bani%20-%20Toyi%20Entekhabam.mp3";
const kUrl2 =
    "http://dl.nex1music.ir/1397/11/13/Behnam%20Bani%20-%20Toyi%20Entekhabam%20[128].mp3?time=1550834085&filename=/1397/11/13/Behnam%20Bani%20-%20Toyi%20Entekhabam%20[128].mp3";

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: AudioApp(),
      ),
    );
  }
}

enum PlayerState { stopped, playing, paused }

class AudioApp extends StatefulWidget {
  @override
  _AudioAppState createState() => _AudioAppState();
}

class _AudioAppState extends State<AudioApp> with WidgetsBindingObserver {
  static const platform = const MethodChannel('app.channel.shared.data');
  String dataShared;

  Duration duration;
  Duration position;

  AudioPlayer audioPlayer;

  String localFilePath;

  PlayerState playerState = PlayerState.stopped;

  get isPlaying => playerState == PlayerState.playing;

  get isPaused => playerState == PlayerState.paused;

  get durationText =>
      duration != null ? duration.toString().split('.').first : '';

  get positionText =>
      position != null ? position.toString().split('.').first : '';

  bool isMuted = false;

  bool shouldRestart = false;

  StreamSubscription _positionSubscription;
  StreamSubscription _audioPlayerStateSubscription;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    initAudioPlayer();

    _loadLocalFile();
    _getSharedUri();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) _getSharedUri();
  }

  @override
  void dispose() {
    _positionSubscription.cancel();
    _audioPlayerStateSubscription.cancel();
    audioPlayer.stop();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void initAudioPlayer() {
    audioPlayer = AudioPlayer();
    _positionSubscription = audioPlayer.onAudioPositionChanged
        .listen((p) => setState(() => position = p));
    _audioPlayerStateSubscription =
        audioPlayer.onPlayerStateChanged.listen((s) {
      if (s == AudioPlayerState.PLAYING) {
        setState(() => duration = audioPlayer.duration);
      } else if (s == AudioPlayerState.STOPPED) {
        onComplete();
        setState(() {
          position = duration;
        });
      }
    }, onError: (msg) {
      setState(() {
        playerState = PlayerState.stopped;
        duration = Duration(seconds: 0);
        position = Duration(seconds: 0);
      });
    });
  }

  Future _play() async {
    await audioPlayer.play(dataShared != null ? dataShared : localFilePath,
        isLocal: true);
    setState(() {
      playerState = PlayerState.playing;
    });
  }

  Future _playLocal() async {
    await audioPlayer.play(localFilePath, isLocal: true);
    setState(() => playerState = PlayerState.playing);
  }

  Future pause() async {
    await audioPlayer.pause();
    setState(() => playerState = PlayerState.paused);
  }

  Future stop() async {
    await audioPlayer.stop();
    setState(() {
      playerState = PlayerState.stopped;
      position = Duration();
    });
  }

  Future mute(bool muted) async {
    await audioPlayer.mute(muted);
    setState(() {
      isMuted = muted;
    });
  }

  void onComplete() {
    setState(() => playerState = PlayerState.stopped);
  }

  Future<Uint8List> _loadFileBytes(String url, {OnError onError}) async {
    Uint8List bytes;
    try {
      bytes = await readBytes(url);
    } on ClientException {
      rethrow;
    }
    return bytes;
  }

  Future _loadFile() async {
    final bytes = await _loadFileBytes(kUrl,
        onError: (Exception exception) =>
            print('_loadFile => exception $exception'));

    final dir = await getApplicationDocumentsDirectory();
    final file = File('${dir.path}/audio.mp3');

    await file.writeAsBytes(bytes);
    if (await file.exists())
      setState(() {
        localFilePath = file.path;
      });
  }

  Future _loadLocalFile() async {
    final dir = await getApplicationDocumentsDirectory();
    final file = File('${dir.path}/audio.mp3');

    if (await file.exists())
      setState(() {
        localFilePath = file.path;
      });
  }

  Future _getSharedUri() async {
    var sharedData = await platform.invokeMethod("getSharedText");
    if (sharedData != null) {
      if (dataShared != sharedData) {
        await stop();
        audioPlayer.play(sharedData, isLocal: true);
        setState(() {
          dataShared = sharedData;
          playerState = PlayerState.playing;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        elevation: 2.0,
        color: Colors.grey[200],
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            children: [
              Material(child: _buildPlayer()),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RaisedButton(
                      onPressed: () =>
                          localFilePath != null ? null : _loadFile(),
                      child: Text(localFilePath != null
                          ? 'already downloaded!'
                          : 'Download 320'),
                    ),
                    RaisedButton(
                      onPressed: () => _playLocal(),
                      child: Text('play local'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPlayer() => Container(
        padding: EdgeInsets.all(16.0),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          Row(mainAxisSize: MainAxisSize.min, children: [
            IconButton(
                onPressed: isPlaying ? null : () => _play(),
                iconSize: 64.0,
                icon: Icon(Icons.play_arrow),
                color: Colors.cyan),
            IconButton(
                onPressed: isPlaying ? () => pause() : null,
                iconSize: 64.0,
                icon: Icon(Icons.pause),
                color: Colors.cyan),
            IconButton(
                onPressed: isPlaying || isPaused ? () => stop() : null,
                iconSize: 64.0,
                icon: Icon(Icons.stop),
                color: Colors.cyan),
          ]),
          duration == null
              ? Container()
              : Slider(
                  value: position?.inMilliseconds?.toDouble() ?? 0.0,
                  onChanged: (double value) {
                    setState(() {
                      position = Duration(seconds: (value ~/ 1000));
                    });
                  },
                  onChangeStart: (double value) {
                    playerState == PlayerState.playing
                        ? shouldRestart = true
                        : null;
                    playerState != PlayerState.stopped
                        ? audioPlayer.pause()
                        : null;
                  },
                  onChangeEnd: (double value) {
                    audioPlayer.seek((value / 1000).roundToDouble());
                    shouldRestart ? _play() : null;
                    shouldRestart = false;
                  },
                  min: 0.0,
                  max: duration.inMilliseconds.toDouble()),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButton(
                  onPressed: () => mute(true),
                  icon: Icon(Icons.headset_off),
                  color: Colors.cyan),
              IconButton(
                  onPressed: () => mute(false),
                  icon: Icon(Icons.headset),
                  color: Colors.cyan),
            ],
          ),
          Row(mainAxisSize: MainAxisSize.min, children: [
            Padding(
                padding: EdgeInsets.all(12.0),
                child: Stack(children: [
                  CircularProgressIndicator(
                      value: 1.0,
                      valueColor: AlwaysStoppedAnimation(Colors.grey[300])),
                  CircularProgressIndicator(
                    value: position != null && position.inMilliseconds > 0
                        ? (position?.inMilliseconds?.toDouble() ?? 0.0) /
                            (duration?.inMilliseconds?.toDouble() ?? 0.0)
                        : 0.0,
                    valueColor: AlwaysStoppedAnimation(Colors.cyan),
                    backgroundColor: Colors.yellow,
                  ),
                ])),
            Text(
                position != null
                    ? "${positionText ?? ''} / ${durationText ?? ''}"
                    : duration != null ? durationText : '',
                style: TextStyle(fontSize: 24.0))
          ]),
        ]),
      );
}
